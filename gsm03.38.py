#!/usr/bin/python3

# -*- coding: utf8 -*- 

import binascii


gsm = ("@£$¥èéùìòÇ\nØø\rÅåΔ_ΦΓΛΩΠΨΣΘΞ\x1bÆæßÉ !\"#¤%&'()*+,-./0123456789:;<=>?"
       "¡ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÑÜ`¿abcdefghijklmnopqrstuvwxyzäöñüà")
ext = ("````````````````````^```````````````````{}`````\\````````````[~]`"
       "|````````````````````````````````````€``````````````````````````")


def encode(plaintext):
    """Return GSM 03.38 7-Bit encoded input"""
    result = []
    for char in plaintext:
        # Try to find char in gsm table
        pos = gsm.find(char)
        if pos != -1:
            result.append(chr(pos))
            continue
        # else try to find char in extended gsm table
        pos = ext.find(char)
        if pos != -1:
            result.append(chr(0x1b))
            result.append(chr(pos))
        else:
            result.append(chr(0x60))
    return ''.join(result)

def decode(code):
    """Return utf-8 encoded GSM 03.38 7-Bit coded input"""
    result = ""
    is_extended = False
    for char in code:
        if char == '\x1b':
            is_extended = True
            continue
        if is_extended:
            is_extended = False
            result += ext[ord(char)]
        else:
            result += gsm[ord(char)]
    return result


def pack(unpacked):
    """Pack GSM03.38 7-Bit coded input"""
    packed = ""
    if len(unpacked) % 8 == 7:
        unpacked += '\x0D' #add <CR>
    unpacked += '\x00' #add padding for message building
    length = len(unpacked)
    message_length = int(length * 7 / 8)
    bitarray = [-1] * message_length
    char = shift = 0

    for n in range(message_length):

        if shift == 6:
            char += 1

        shift = n % 7
        low_part = ord(unpacked[char]) >> shift
        high_part = (ord(unpacked[char+1]) << (7-shift) & 0xff)
        bitarray[n] = low_part + high_part
        char += 1
    packed = bytes([message_length]) + bytes(bitarray)
    return packed

def unpack(packed):
    """Unpack GSM03.38 7-Bit coded input"""
    #print(packed[0])
    packed_length = packed[0]
    packed = b'\x00' + packed[1:] + b'\x00'
    unpacked_length = int(packed_length*8/7)
    unpacked_message = [-1] * unpacked_length
    line = shift = 0
    #print("Packed: %d\nUnpacked: %d"%(packed_length, unpacked_length))
    #print("real len: %d"%len(packed))

    for idx in range(unpacked_length):
        if shift == 7:
            line -= 1
        shift = idx % 8
        #print("line: %d"%line)
        #print("shift: %d"%shift)
        #print("idx: %d"%idx)
        low = packed[line] >> (8 - shift)
        #print("low: %s"%hex(low))
        high = (packed[line+1] & ((2**(8-shift))-1)) << shift
        #print("high: %s"%hex(high))
        
        line+=1

        unpacked_message[idx] = chr((low + high) & 0b01111111)
        #print("comb: %s [%s]\n"%(hex(ord(unpacked_message[idx])),unpacked_message[idx]))

    return ''.join(unpacked_message)


def ussd_encode(plain):
    """Return hexlified GSM03.38 encoded and packed input"""
    return binascii.hexlify(pack(encode(plain)))

def ussd_decode(code):
    """Return utf-8 decoded GSM03.38 coded input"""
    return decode(unpack(binascii.unhexlify(code)))


if __name__ == "__main__":
    """ Make basic usecase callable"""

    import argparse

    parser = argparse.ArgumentParser(description='GSM03.38 De/Encoding Tool.')
    parser.add_argument('Mode', choices=['encode','decode'], help='Mode to use')
    parser.add_argument('Input', nargs='+', help='Text to de/encode')
    args = parser.parse_args()

    if args.Mode == "encode":
        for Input in args.Input:
            print(ussd_encode(Input).decode('utf-8'))

    if args.Mode == "decode":
        for Input in args.Input:
            print(ussd_decode(Input).decode('utf-8'))

